FROM 10.7.2.45:5000/qa/base-rule-trt:0.1.2-devel

ARG gituser=izzand
ARG gitpasswd=nodeflux


WORKDIR /opt

RUN \ 
    apt-get update &&\
    apt install -y git &&\
    apt-get install -y wget



RUN \
  apt-get -y install python-pip python-dev build-essential

# DSLIB For Specific Rule

WORKDIR /tmp
# install local utils
RUN \
  pip2 install git+https://$gituser:$gitpasswd@bitbucket.org/verysmartdonkey/nodeflux-logger-python#egg=logger && \
  pip3 install git+https://$gituser:$gitpasswd@bitbucket.org/verysmartdonkey/nodeflux-logger-python#egg=logger && \
  pip3 install git+https://$gituser:$gitpasswd@bitbucket.org/verysmartdonkey/io-utils.git --upgrade && \
  pip3 install git+https://$gituser:$gitpasswd@bitbucket.org/verysmartdonkey/math-utils.git --upgrade && \
  pip3 install git+https://$gituser:$gitpasswd@bitbucket.org/verysmartdonkey/image-utils.git --upgrade && \
  pip3 install --upgrade git+https://$gituser:$gitpasswd@bitbucket.org/verysmartdonkey/detect-and-track.git --upgrade && \
  git clone https://$gituser:$gitpasswd@bitbucket.org/verysmartdonkey/processing-utils.git && \
  cd processing-utils && \
  git checkout v0.5.1 && \
  python3 setup.py install && \
  cd .. && \
  git clone https://$gituser:$gitpasswd@bitbucket.org/verysmartdonkey/base-analytics.git && \
  cd  base-analytics && \
  python3 setup.py install && \
  cd .. && \
  git clone https://$gituser:$gitpasswd@bitbucket.org/verysmartdonkey/object-line-counter.git && \
  cd object-line-counter && \
  python3 setup.py install

# install dependencies
RUN \
  apt-get install -y libspdlog-dev &&\
  pip2 install pytz && \
  pip3 install pytz && \
  pip3 install psycopg2 && \
  pip3 install boto3==1.7.75  && \
  pip3 install numpy==1.14.2 &&\
  pip3 install scipy==1.0.0 && \
  pip3 install pandas && \
  pip3 install numba==0.39.0 &&\
  pip3 install filterpy==1.4.2 &&\
  pip3 install pytz && \
  pip3 install requests && \
  pip3 install tornado &&\
  pip3 install requests &&\
  pip3 install python-dotenv &&\
#  apt-get install -y python-tk &&\
  pip3 install pillow && \
  pip3 install dlib && \
  pip3 install scikit-image && \
  pip install pillow && \
  pip3 install pika

RUN \
  mkdir -p /root/.nodeflux/rules
#  mkdir -p /root/.nodeflux/tmp


WORKDIR /opt
RUN \
  apt -y install python3-pip && \
  git clone https://$gituser:$gitpasswd@bitbucket.org/verysmartdonkey/nodeflux-local-coordinator.git && \
  cd nodeflux-local-coordinator && \
  git checkout master

RUN \
  pip3 install git+https://$gituser:$gitpasswd@bitbucket.org/verysmartdonkey/nodeflux-logger-python#egg=logger && \
  pip3 install psutil 

WORKDIR /tmp

RUN \
  git clone https://$gituser:$gitpasswd@bitbucket.org/verysmartdonkey/ipc-utils.git && \
  cd ipc-utils && \
  git checkout master && \
  python3 setup.py install && \
  git checkout v1.0.0 && \
  python setup.py install

WORKDIR /tmp

RUN \
  git clone https://$gituser:$gitpasswd@bitbucket.org/verysmartdonkey/vehicle-traffic-surveillance.git && \
  cd vehicle-traffic-surveillance && \
  git checkout nfinfer_openlpr && \
  python3 setup.py install

WORKDIR /root/.nodeflux/rules

RUN \
  git clone https://$gituser:$gitpasswd@bitbucket.org/verysmartdonkey/rule-license-plate-recognition.git && \
  cd rule-license-plate-recognition && \
  git checkout v3.1.0 && \
  cd .. && \
  git clone https://$gituser:$gitpasswd@bitbucket.org/verysmartdonkey/rule-vehicle-traffic-surveillance.git && \
  cd rule-vehicle-traffic-surveillance && \
  git checkout v3.1.0

WORKDIR /opt/nodeflux-local-coordinator

RUN \
  mkdir -p /root/.nodeflux/tmp

RUN \
  pip3 install pycuda

EXPOSE 9000-9100

CMD ["python3", "main.py"]

