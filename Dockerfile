FROM 411371596927.dkr.ecr.ap-southeast-1.amazonaws.com/nodeflux/opencv:3-cuda-8.0-cudnn6-devel-ubuntu16.04

WORKDIR /opt

RUN \ 
    apt-get update &&\
    apt-get install -y wget

RUN \
    wget https://s3-ap-southeast-1.amazonaws.com/nodeflux-data/cudnn/cudnn-8.0-linux-x64-v7.tgz &&\
    tar -xvf cudnn-8.0-linux-x64-v7.tgz

# Preparing cmake

WORKDIR /tmp/cmake

RUN \
    apt-get purge -y cmake &&\
    wget https://cmake.org/files/v3.12/cmake-3.12.0-Linux-x86_64.sh &&\
    chmod +x cmake-3.12.0-Linux-x86_64.sh &&\
    ./cmake-3.12.0-Linux-x86_64.sh --skip-license &&\
    cp -r bin/* /usr/bin/. &&\
    cp -r share/* /usr/share/.

#install nf-darknet

WORKDIR /app

RUN \
    apt-get install -y libspdlog-dev &&\
    git clone https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/nf-darknet.git &&\
    cd nf-darknet &&\
    git checkout v1.1.7 &&\
    rm -r .git &&\
    cd data && wget https://s3-ap-southeast-1.amazonaws.com/nodeflux-data/nodeflux-zoo/yolo-v2-old/yolo_ori/yolo.weights &&\
    cd .. && mkdir build && cd build &&\
    cmake \
    -DGPU=1 \
    -DCUDNN=1 \
    -DOPENMP=1 \
    -DCUDNN_STATIC=1 \
    -DCUDNN_LIB_PATH=/opt/cuda/lib64 \
    -DCUDNN_INCLUDE_PATH=/opt/cuda/include \
    -DDARKNET_C_STATIC=1 \
    -DBUILD_EXAMPLES=1 .. &&\
    make -j$(getconf _NPROCESSORS_ONLN) &&\
    make install &&\
    cd .. && rm -r data

#install nf-predictor

WORKDIR /app/

RUN \
  git clone https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/nf-predictor.git && \
  cd nf-predictor && \
  git checkout v0.1.2 && \
  mkdir build && \
  cd build && \
  cmake .. && \
  make -j8 && \
  make install

# install grabhw
WORKDIR /tmp
RUN \
  export LC_ALL=C
RUN \
  apt update && \
  apt-get -y install libx265-dev && \
  apt-get -y  install libsdl1.2-dev && \
  apt-get -y  install libvdpau-dev && \
  apt-get -y  install libva-dev && \
  apt-get -y  install yasm && \
  cp -rf /usr/local/cuda-8.0/lib64/stubs/libcuda.so /usr/lib/x86_64-linux-gnu/. && \
  export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu:/usr/local/cuda/lib64:$LD_LIBRARY_PATH &&\
  git clone https://github.com/libav/libav.git && \
  cd libav && git checkout release/12 && \
  ./configure --extra-cflags=-I/usr/local/cuda/include --extra-ldflags=-L/usr/local/cuda/lib64 --enable-nonfree --enable-libnpp --enable-libx265 --enable-gpl --enable-shared  --enable-vdpau  && \
  make all -j8  && \
  make install && \
  ldconfig -v && \
  cd .. && \
  rm -r libav

WORKDIR /tmp

RUN \
  ls && \
  cp -rf /usr/local/cuda-8.0/lib64/stubs/libcuda.so /usr/lib/x86_64-linux-gnu/. && \
  git clone https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/nodeflux-avlib.git && \
  cd nodeflux-avlib && \
  git checkout v1.0.3 && \
  export LD_LIBRARY_PATH=/usr/lib/x86_64-linux-gnu:/usr/local/cuda/lib64:$LD_LIBRARY_PATH &&\
  make && \
  make install && \
  make grabhw && \
  mkdir -p /root/.nodeflux && \
  cp -rf /tmp/nodeflux-avlib/VERSION /root/.nodeflux && \
  cp -rf /tmp/nodeflux-avlib/grabav.py /root/.nodeflux && \
  cp -rf /tmp/nodeflux-avlib/grabhw /root/.nodeflux && \
  cd .. && \
  rm -r nodeflux-avlib


RUN \
  apt-get -y install python-pip python-dev build-essential

# DSLIB For Specific Rule

WORKDIR /tmp

RUN \
#  pip install --upgrade pip &&\
  apt-get install -y libspdlog-dev &&\
  pip install pytz && \
  pip install git+https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/nodeflux-logger-python#egg=logger
RUN \
  pip install git+https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/io-utils.git --upgrade && \
  pip install git+https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/math-utils.git --upgrade && \
  pip install git+https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/image-utils.git --upgrade
RUN \
  pip install --upgrade git+https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/detect-and-track.git --upgrade
RUN \
  git clone https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/processing-utils.git && \
  cd processing-utils && \
  git checkout add_timestamp&& \
  python setup.py install && \
  git clone https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/ipc-utils.git && \
  cd ipc-utils && \
  git checkout change_cvt_raw_output && \
  pip install -I git+https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/base-analytics.git --upgrade && \
  pip install git+https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/object-line-counter.git --upgrade &&\
  pip install -I  git+https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/vehicle-traffic-surveillance.git --upgrade

RUN \
  pip install psycopg2 && \
  pip install boto3==1.7.75  && \
  pip install dlib==19.10 
RUN \
  pip install mxnet-cu80==1.2.0 &&\
  pip install numpy==1.14.2 &&\
  pip install scipy==1.0.0 && \
  pip install pandas
RUN \
  pip install numba==0.39.0 &&\
  pip install filterpy==1.4.2 &&\
  pip install pytz && \
  pip install requests

RUN \
  pip install tornado &&\
  pip install requests &&\
  pip install python-dotenv &&\
  pip install pika &&\
  pip install cassandra-driver &&\
  pip install pymongo && \
  apt-get install -y python-tk

RUN \
  mkdir -p /root/.nodeflux/rules
#  mkdir -p /root/.nodeflux/tmp

WORKDIR /root/.nodeflux

#install stream-server

RUN \
  git clone https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/stream-server.git && \
  cd stream-server && \
  git checkout connection_error_handling

WORKDIR /opt

#install stream-server

RUN \
  apt -y install python3-pip && \
  git clone https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/nodeflux-local-coordinator.git

RUN \
  pip3 install git+https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/nodeflux-logger-python#egg=logger && \
  pip3 install psutil && \
  pip3 install gputil && \
  pip3 install tensorflow-gpu==1.4.1

WORKDIR /opt/nodeflux-local-coordinator

RUN \
  pip3 install git+https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/nodeflux-logger-python#egg=logger && \
  pip3 install psutil && \
  pip3 install gputil

WORKDIR /tmp

RUN \
#  git clone https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/processing-utils.git && \
  git clone https://wmpandrian:tbdm1234@bitbucket.org/verysmartdonkey/ipc-utils.git && \
  cd ipc-utils && \
  git checkout master && \
  python setup.py install

WORKDIR /opt/nodeflux-local-coordinator

RUN \
  mkdir -p /root/.nodeflux/tmp

EXPOSE 9000-9100

CMD ["python3", "main.py"]
