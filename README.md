# Nodeflux License Plate Recognition #

This repository consist of the implementation of license plate recognition rule designed to run on Nodeflux IVA Platform.

## Feature ##

- Plate recognition.

## Dependencies ###

- rabbitmq server
- python 3.5
- Nodeflux-utils (detail can be obtained in dockerfile)
- Nodeflux tensorRT
- Nodeflux plate-recognition-services
- for details see Dockerfile

## Configuration ##

| parameter name              | details                                                      | type            | default value                                                                              |
|-----------------------------|--------------------------------------------------------------|-----------------|--------------------------------------------------------------------------------------------|
| vehicle_cfg                 | full path to cfg file                                        | string          | 11                                                                                         |
| vehicle_names               | full path to cfg file                                        | string          | /root/nodeflux-zoo/yolo-v2-old/vehicle_detection/vehicle_detection_v2-1.names              |
| vehicle_threshold           | vehicle detection threshold float 0 to 1                     | float           | 0.2                                                                                        |
| vehicle_plan                | full path to cfg file                                        | string          | /root/nodeflux-zoo/yolo-v2-old/vehicle_detection/vehicle_detection_yolo_best_v4_int32.plan |
| vehicle_nms                 | nms value 0 to 1                                             | float           | 0.2                                                                                        |
| use_gpu_from_db             | if true discard round robin                                  | bool            | false                                                                                      |
| gpu_id                      | gpu index to be used                                         | integer         | 0                                                                                          |
| cam_id                      | cam id in integer                                            | integer         | 1                                                                                          |
| max_frame_age               | maximum frame number of object to be decided as dead tracker | integer         | 10                                                                                         |
| object_counter_flag_two_way | detect and count object in two direction or not              | bool            | true                                                                                       |
| counting line               | counting line                                                | array of points | None                                                                                       |
| rescale_bbox_factor         | scale object detector in [x,y]                               | tuple of float  | [1,1]                                                                                      |
| object_center               | define object movement                                       | string          | center                                                                                     |
| tracker_code                | define tracker code                                          | string          | sort                                                                                       |
| filtered_labels             | filter object name to be detected                            | list            | ["car" ,"bus" ,"truck" ,"small" ,"medium" ,"big" ,"motorcycle" ]                           |
| api_url                     | api dump url                                                 | string          | None                                                                                       |
| rabbit_mq_ip_address        | rabbit mq address                                            | string          | localhost                                                                                  |
| queue_name                  | rabbit mq queue name                                         | string          | license_plate_recognition                                                                  |
| frame_resize                | resize factor for visualization image                        | float           | 0.5                                                                                        |

### Architecture Overview ###

Overview architecture can be found in nodeflux technical docs.
<https://nodeflux.atlassian.net/wiki/spaces/IE/pages/747765843/rule-vehicle-traffic-surveillance>