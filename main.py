# author:kiki
from nodeflux.logger import Logger
from os import sys
from scipy.spatial import distance
from __init__ import __version__
from nodeflux.analytics.vehicle_traffic_surveillance_monitoring import *
from nodeflux.io_utils.file_function import *
import os
from nodeflux.post_processing.post_processing_nfinfer import *
import cv2
from os.path import expanduser
import json
import requests
from threading import Thread
import pytz
import detection_pb2
global conn
global cur
import numpy as np
from datetime import datetime
global param_inputs
import time
import base64
import numpy as np
import pika
import multiprocessing as mp

temp_dir_path = os.path.join(expanduser("~")+"/.nodeflux","tmp")
logger = Logger('main')

class LicensePlateRecognitionMain(PostProcessing):
    ''''
    Vehicle Traffic Surveillance
    -----------------------------
    pipeline-1 =  - Vehicle-classification (as a main rule)
                - LPR (as a feature) , this contains plate detection and char recognition
                - Object-counter (as a feature), we use rule counter 'IN' (one direction),
                                when set params 'flag_two_ways=True', the rule will count from 2 ways
                - traffic-monitoring (as a feature), monitoring vehicle surveillance in ranges 0 until 4

    pipeline-2 =  - LPR (as a main rule) , this contains plate detection and char recognition
                - Object-counter (as a feature), we use rule counter 'IN' (one direction),
                                when set params 'flag_two_ways=True', the rule will count from 2 ways
                - traffic-monitoring (as a feature), monitoring vehicle surveillance in ranges 0 until 4

    Note : The feature can set a value as 'TRUE' (active) or 'FALSE'(Non)

    '''

    def dump_rabbit_mq(self, item):

        image_dump = item['attribute']['image_vehicle']

        dump_data = detection_pb2.BatchDetection()
        dump_data.sources.append(self.sources)
        frame_data = dump_data.frames.add()
        frame_data.timestamp.FromDatetime(item['attribute']["timestamp_frame"].replace(tzinfo=None))

        detected_object = frame_data.objects.add()
        detected_object.label_name = item['attribute']['class']

        thumbnail_data = detected_object.thumbnail
        thumbnail_data.format = detection_pb2.ImageArray.HWC
        thumbnail_data.data = cv2.imencode(
        '.jpg', image_dump)[1].tobytes()

        detected_object.box.x1 = item['attribute']['plate_bbox'][0]
        detected_object.box.x2 = item['attribute']['plate_bbox'][2]
        detected_object.box.y1 = item['attribute']['plate_bbox'][1]
        detected_object.box.y2 = item['attribute']['plate_bbox'][3]

        for shape in image_dump.shape:
            thumbnail_data.shape.append(shape)
        serialized_dump_data = dump_data.SerializeToString()

        try:
            connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=self.rabbit_mq_ip_address))
            channel = connection.channel()
            channel.queue_declare(queue=self.queue_name)
            channel.basic_publish(
            exchange='',
            routing_key=self.queue_name,
            body=serialized_dump_data,
            properties=pika.BasicProperties(delivery_mode=1, ))
            connection.close()
        except Exception as e:
            logger.info(e)

    def send_to_plate_recognition_services(self):
        while True:
            time.sleep(0.1)
            for i,data_frame in reversed(list(enumerate(self.dump_mq))):
                data_frame['attribute']['plate_bbox'] = [0,0,0,0]
                if data_frame['attribute']['plate_bbox'] is not None:
                    self.dump_rabbit_mq(data_frame)
                try:
                    del self.dump_mq[i]
                except:
                    logger.info("failed to delete list")

    def __init__(self, 
                main_detector=None,
                configuration = {}
                ):
        '''
        initialize default main detector
        '''
        self.rule_detect = main_detector
        self.cam_id = configuration['cam_id']
        self.rule_type = "rule-vehicle-traffic-surveillance"

        sources = dict()
        sources['cam_id'] = self.cam_id 
        sources['rule_type'] = self.rule_type

        self.sources = json.dumps(sources)

        '''
        initialize default image in case image is corrupt
        '''
        self.img = None
        self.default_image = np.zeros([100, 100, 3], dtype=np.uint8)
        retval, self.default_base64 = cv2.imencode('.jpg',self.default_image)
        self.buffer64 = base64.b64encode(self.default_base64)

        '''
        initialize mq connection config
        '''
        self.rabbit_mq_ip_address = configuration['rabbit_mq_ip_address']
        self.queue_name = configuration['queue_name']

        '''
        initialize api for direct dumping in case plate is not detected
        '''
        self.dump_api = configuration['dump_api']

        '''
        initialize list declaration
        '''
        self.dump_db_license_plate = []
        self.dump_db_traffic_density = []
        self.thread_list = []
        self.dump_mq = []

        '''
        initialize post processing
        '''
        PostProcessing.__init__(self,
                                enable_debug = False,
                                fmmap_out=configuration['fmmap_out'],
                                mmap_size=int(configuration['mmap_size']),
                                use_thread=True
                                )
        
        '''
        initialize thread
        '''
        self.init_thread()



    def init_thread(self):
        '''
        initialize thread
        '''

        thr_send_to_plate_recognition_services = Thread(target=self.send_to_plate_recognition_services, args=())
        thr_send_to_plate_recognition_services.start()

        self.thread_list.append(thr_send_to_plate_recognition_services)

    def check_thread(self):
        for thr in self.thread_list:
            if thr.is_alive() is False:
                exit()
    
    def check_health(self):
        self.check_thread()

    def pre_processing(self, ori_img):


        return ori_img


    def run_processing(self, img, json_data, timestamp,callback=None):

        self.check_health()

        self.img=img
        self.rule_detect.add_data(data=json_data, video_size=(img.shape[0], img.shape[1]),timestamp_frame=timestamp)
        self.rule_detect.add_render_frame(frame=img)
        self.rule_detect.process_data()

        dumped_list = self.rule_detect.result_data()
        if dumped_list[0] is not None:
            if dumped_list[0] is not None:
                for data in dumped_list[0]:
                    self.dump_mq.append(data)
        '''
        # Object counter and LPR example Output :
        [{'attribute': {'image_plate': array([[]], dtype=uint8),'rule': 'LPR',
        'plate_no': 'B1057WOK', 'image_vehicle': array([[]], dtype=uint8),
        'class': 'BUS',
        'id_area': ''}}]

        # Traffic monitoring example output:

        [{'attribute': {'rule': 'TRAFFIC-MONITORING', 'categorization': 'TEST', 'summarize_weight': 1.8, 'id_area': 0, 'image_roi':np.array[---]}},
         {'attribute': {'rule': 'TRAFFIC-MONITORING', 'categorization': 'TEST', 'summarize_weight': 1.8, 'id_area': 1,'image_roi':np.array[---]}}
        '''


    def get_visualization(self):
        return  self.rule_detect.result_render_frame()

def get_image_size(mem_name,mem_size):
    mem_input = MemoryInterface(mem_name, mem_size=int(mem_size))
    mem_input.setup_mmap()
    logger.info("Probe image size")
    counter = 0
    while True:
        mem_input.flush_mmap()
        width = mem_input.get_img_width_from_grabst()
        height = mem_input.get_img_height_from_grabst()
        if width == 1280:
            if height == 720:
                logger.info("Got image size, width = "+str(width)+" ,height = "+str(height))
                return width, height
        if width == 1920:
            if height == 1080:
                logger.info("Got image size, width = "+str(width)+" ,height = "+str(height))
                return width, height
        time.sleep(3)
        counter = counter+1
        if counter == 10:
            logger.info("failed to get frame size, either the camera is off, or the analytics did't receive required resolution 1080p or 720p, please check camera availability and setting")
            sys.exit()

def get_param_request(api_url):
    try:
        result = requests.get(url = api_url)
    except:
        return False
    return json.loads(result.text)

def get_params_input():
    data = {}
    trx_id = sys.argv[1]
    param_url = sys.argv[6]
    '''
    get param from database
    '''

    result = get_param_request(param_url)

    if result is not False:

        data['configuration'] = {}
        data['configuration']['flag_feature_classification'] = True 
        data['configuration']['flag_feature_lpr'] = False
        data['configuration']['flag_feature_traffic_monitoring'] = False 
        data['configuration']['flag_counting'] = True
        data['configuration']['flag_speed'] = False
        data['configuration']['flag_visualization'] = True

        """
        Configure Model path: VEHICLE
        ========================
        """

        gpu_id = int(sys.argv[5])
        
        if result['additional_param']['use_gpu_from_db'] is True:
            gpu_id = result['additional_param']['gpu_id']    

        data['configuration']['frame_resize'] = result['additional_param']['frame_resize']
        data['model_inferences'] = {}
        model_inferece = {}
        model_inferece['framework_name'] = FRAMEWORK_DARKNET
        model_inferece['model_name'] = MODEL_VEHICLE
        model_inferece['data'] = None
        model_inferece['cfg'] = result['additional_param']['vehicle_cfg'] 
        model_inferece['weight'] = None
        model_inferece['names'] = result['additional_param']['vehicle_names']
        model_inferece['threshold'] = result['additional_param']['vehicle_threshold']
        model_inferece['gpu_id'] =  gpu_id
        model_inferece['nfdarknet'] = CODE_NF_INFER
        model_inferece['plan'] = result['additional_param']['vehicle_plan']
        model_inferece['nms'] = result['additional_param']['vehicle_nms']
        model_inferece['width'] = 14
        model_inferece['height'] = 14
        data['model_inferences'][FRAMEWORK_DARKNET + '_' + MODEL_VEHICLE] = model_inferece

        # """
        # Configure Model path: PLATE
        # Not used anymore but still need to fill the value
        # ========================
        # """

        parent_path_model_plate = os.path.expanduser('~') + '/nodeflux-zoo/yolo-v2-old/plate_new/'
        __data_plate = parent_path_model_plate + 'license_plate.data'
        __cfg_plate = parent_path_model_plate + 'inf_tiny-license_plate.cfg'
        __weight_plate = parent_path_model_plate + 'license_plate_best.weights'
        __namelist_plate = parent_path_model_plate + 'license_plate.names'
        __model_plan = parent_path_model_plate + 'license_plate_best_v2_int32.plan'
        __gpu_id_plate = int(sys.argv[5])

        __th_plate = 0.2

        model_inferece = {}
        model_inferece['flag_landmark'] = False
        model_inferece['path_dlib_landmark'] = ""
        model_inferece['framework_name'] = FRAMEWORK_DARKNET
        model_inferece['model_name'] = MODEL_PLATE
        model_inferece['data'] = None
        model_inferece['cfg'] = ""
        model_inferece['weight'] = None
        model_inferece['names'] = ""
        model_inferece['threshold'] = __th_plate
        model_inferece['gpu_id'] = int(sys.argv[5])
        model_inferece['nfdarknet'] = CODE_NF_INFER
        model_inferece['plan'] = ""
        model_inferece['nms'] = 0.4
        model_inferece['width'] = 14
        model_inferece['height'] = 14
        data['model_inferences'][FRAMEWORK_DARKNET + '_' + MODEL_PLATE] = model_inferece

        # """
        # Configure Model path: CHAR
        # Not used anymore but still need to fill the value
        # ========================
        # """

        model_inferece = {}
        model_inferece['framework_name'] = FRAMEWORK_DARKNET
        model_inferece['model_name'] = MODEL_CHAR
        model_inferece['data'] = None
        model_inferece['cfg'] = None
        model_inferece['weight'] = None
        model_inferece['names'] = None
        model_inferece['names'] = None
        model_inferece['threshold'] = None
        model_inferece['gpu_id'] =  None
        model_inferece['nfdarknet'] = CODE_NF_INFER
        model_inferece['plan'] = None
        model_inferece['nms'] = 0.3
        model_inferece['width'] = 14
        model_inferece['height'] = 7
        data['model_inferences'][FRAMEWORK_DARKNET + '_' + MODEL_CHAR] = model_inferece


        data['rule_name'] = 'RULE_ASSIGNMENT_VEHICLE_TRAFFIC_SURVEILLANCE'
        data['end_point'] = None

        fmmap_in = sys.argv[2]
        fmmap_out = sys.argv[3]
        mmap_size = sys.argv[4]

        width, height = get_image_size(fmmap_in,mmap_size)

        roi_list = []

        area = []
        count = 1
        if "counting_line" in result['additional_param'].keys():
            selected_roi = result['additional_param']['counting_line'][0]['points']
            for item in selected_roi:
                pts = (int(item['x']*width), int(item['y']*height))
                area.append(pts)
                if count%2 == 0:
                    roi_list.append(area)
                    area = []
                count = count+1
        elif "roi" in result['additional_param'].keys():
            for item in result['additional_param']['roi']:
                for point in item:
                    pts = (int(point['x']*width), int(point['y']*height))
                    area.append(pts)
                    if count%2 == 0:
                        roi_list.append(area)
                        area = []
                    count = count+1
        else:
            sys.exit("roi/counting line not found")

        data['configuration']['cam_id'] =int(result['cam_id'])
        data['fps_inferencing'] = 25
        data['configuration']['max_frame_age'] = int(result['additional_param']['max_frame_age'])
        data['configuration']['object_counter_flag_two_way'] = result['additional_param']['object_counter_flag_two_way']
        data['configuration']['lines'] = roi_list
        data['configuration']['rescale_bbox_factor'] =float(result['additional_param']['rescale_bbox_factor']) 
        data['configuration']['object_center'] = result['additional_param']['object_center']
        data['configuration']['tracker_code'] = result['additional_param']['tracker_code']
        data['configuration']['max_overlap_char']= 10
        data['configuration']['min_data_char']= 10
        data['configuration']['max_data_char']= 10
        data['configuration']['traffic_time_save'] =10
        data['configuration']['real_distance']= [30]
        data['configuration']['flag_watermark_logo'] = True
        data['configuration']['feature_name'] = 'VEHICLE TRAFFIC SURVEILLANCE| property of nodeflux'
        data['configuration']['labels']=result['additional_param']["filtered_labels"]

        data['configuration']['fmmap_in'] = fmmap_in
        data['configuration']['fmmap_out'] = fmmap_out
        data['configuration']['mmap_size'] = int(mmap_size)
        data['configuration']['rabbit_mq_ip_address'] = result['additional_param']['rabbit_mq_ip_address']
        data['configuration']['queue_name'] =result['additional_param']['queue_name']
        data['configuration']['dump_api'] = result['dump_api']
    else:
        sys.exit(1)

    return data

if __name__ == '__main__':

    print("initialize rule vehicle traffic surveillance version : ",__version__)

    param_inputs= get_params_input()
    
    rule_assignment = RuleAssignment()
    flag_success=rule_assignment.checking_rule_assignment_initialization(params=param_inputs)
    param_configuration = param_inputs['configuration']

    fps_inferencing = param_inputs['fps_inferencing']


   
    if not flag_success:
        print ('Error :Please to check input parameter to assign the rules')

    else:
        ###################################################
        # # --- main module --#

        main_detector = VehicleTrafficSurveillance(feature_assignment_names= list(rule_assignment.process_manager.keys()),
                                              labels=param_configuration['labels'],
                                              video_size=None,
                                              patch_size=None,
                                              frame_resize = param_configuration['frame_resize'],
                                              maximum_frame_age=param_configuration['max_frame_age'],
                                              tracker_code=param_configuration['tracker_code'],
                                              object_center=param_configuration['object_center'],  # bottom or center
                                              flag_watermark_logo=True,
                                              rescale_bbox_factor=param_configuration['rescale_bbox_factor'],
                                              feature_name='License Plate Recognition| property of nodeflux', nfdarknet=CODE_NF_INFER
                                              )

        lines=param_configuration['lines']
        j = 0
        for i in range(0, len(lines) - 1, 2):
            line = [lines[i], lines[i + 1]]
            main_detector.add_line(lines=line)
            j += 1
        rule_assignment.run()

        main_rule = LicensePlateRecognitionMain(main_detector=main_detector,
                                                    configuration = param_inputs['configuration']
                                                    )

        frame_grabber = FrameGrabber(
                        enable_debug = False,
                        inf_cfg= rule_assignment.main_detection_params['cfg'],
                        inf_thresh= str(rule_assignment.main_detection_params['threshold']),
                        model_plan = rule_assignment.main_detection_params['plan'],
                        end_point = None,
                        mem_name = param_configuration['fmmap_in'],
                        mem_size = int(param_configuration['mmap_size']),
                        gpu_id=rule_assignment.main_detection_params['gpu_id'],
                        inf_names=rule_assignment.main_detection_params['names']
                        )
        mp_detector = mp.Process(target=frame_grabber.start_detector, args=())
        mp_detector.start()
        main_rule.run()
