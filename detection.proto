syntax = "proto3";

import "google/protobuf/timestamp.proto";

/**
 * A BoundingBox represents a set of coordinates that bound an object
 * in a frame. It is represented in the x and y axis, where it starts
 * from the top left of the frame.
 *
 *                         x1y1     x2y1
 *                           |--------|
 *                           |        |
 *                           |--------|
 *                         x1y2      x2y2
 *
 * It can be represented on raw pixel or normalized to the frame size.
 */
message BoundingBox {
    bool normalized = 1;
    float x1 = 2;
    float y1 = 3;
    float x2 = 4;
    float y2 = 5;
    fixed32 track_id = 6;
}

message Landmarks {
    bool normalized = 1;
    repeated float x = 2;
    repeated float y = 3;
    repeated fixed32 id = 4;
}

message ImageArray {
    bytes data = 1;
    repeated uint32 shape = 2;
    
    enum DataFormat {
        HWC = 0;
        CHW = 1;
    }
    
    enum DataType {
        u8 = 0;
        fp16 = 1;
        fp32 = 2;
    }

    DataFormat format = 3;
    DataType type = 4;
}

/**
 * A DetectedObject provide an information of a single detected object.
 * It consists of the object's BoundingBox, label, class and detection
 * confidence score.
 */
message DetectedObject {
    string object_id = 1;
    BoundingBox box = 2;
    Landmarks landmarks = 3;
    string label_name = 4;
    fixed32 label_id = 5;
    float score = 6;
    ImageArray thumbnail = 7;
}


/**
 * A FrameDetection is a collection of DetectedObject found in a single frame.
 */
message FrameDetection {
    repeated DetectedObject objects = 1;
    google.protobuf.Timestamp timestamp = 2;
    int64 frame_number = 3;
    ImageArray original_frame = 4;
}

/**
 * BatchDetection is a collection of FrameDetection that spans from
 * multiple frame.
 */
message BatchDetection {
    repeated FrameDetection frames = 1;
    repeated string sources = 2;
}
